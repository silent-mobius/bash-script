#!/usr/bin/env bash

########################################################################
#Created by: Silent-Mobius
#Purpose: Exercise
#Date: 01.01.2020
#version: 1.0.0
set -o pipefail # should stop script when a | b, either a or b fails
set -o errexit # should stop script on any error
set -o noclobber # should not allow to over-write script
set -o nounset # do not allow un-used variables
########################################################################


VAR=$1 # passing variable as first parameter


echo "Parameter VAR=$VAR has been passed"


function get_value_from_script(){
    local  VAR=$1
    echo "Function parameter is: ${VAR^^}"
}

get_value_from_script ${VAR}
