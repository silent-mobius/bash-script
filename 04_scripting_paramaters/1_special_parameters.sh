#!/usr/bin/env bash

########################################################################
#Created by: Silent-Mobius
#Purpose: Exercise
#Date: 01.01.2020
#version: 1.0.0
set -o pipefail # should stop script when a | b, either a or b fails
set -o errexit # should stop script on any error
set -o noclobber # should not allow to over-write script
set -o nounset # do not allow un-used variables
########################################################################


echo \$\$ $$  PID of the script	        # echo $$ sign prints process of the ID
echo \$\# $#  count paraments           # echo $# sign will count the entered variables
echo \$\? $?  last cmd exit code        # echo $? sign will print the exit status of last ran cmd
echo \$\* $*  all arguments             # echo $* sign will reprint 
echo \$\@ $@  all arguments             # expand to the value of all the positional parameters combined.
echo \$\_ $_  last argument             # set to the last argument to that command.
echo \$\!  last PID used             # contains the PID of the command most recently executed in the background.
echo \$\- $-  expand option flags  # expands to the current option flags as specified upon invocation, by the set builtin command, or those set by the shell itself

