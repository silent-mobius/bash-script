
---

<center style="background-color:lightgreen">
    <h1 style="color:black"> 
        Bash: Scripting Parameters
    </h1>
</center>

## Data types

Unlike many programming languages, Bash script does not has a pre defined data type. Bash uses a *no data type* system which also can be called as `scalar` in some minor scripting languages.
Despite all mentioned, we can enforce with *declare* command, to set a type to integer, indexed arrays, associated arrays and manipulate attributes. All that *declare* command can not enforce to set to previously mentioned types, we can convert with double quotes or single quotes around the provided data value.
All these data types are used by Bash shell to transfer data to Linux kernel and receives them in the form of special commands called *syscalls*. 
Yet we do not talk about kernel development, only Bash script, thus question is born: How do we send, or to be precise *pass*, data to our shell and shell scripts? Short answer: **parameters**


## There 3 types of parameters

- Positional Parameters: Parameters that are passed down to script based on their position next to the script. Everything in computer starts with zero, thus script position is evaluated as `$0` and any parameter next to it is evaluated as `$1, $2..$9`. In case more then 9 positional parameters are used (bad practice by the way, use getopts instead), one can expand the positional parameters with brace expansion , e.g `${10} ${11}` and so on.
    - [ Positional arguments example ](./0_positional_parameters.sh)

- Special Parameters: Types of parameters that we can get from shell itself, that provide us details about the shell and evaluation of shell itself, things like exit statuses, process ids, amount of parameters and so on. 
    - [ Special arguments example ](./1_special_parameters.sh)

- Variables Parameters: Variables are named symbols that represent either a string or numeric value. When you use them in commands and expressions, they are treated as if you had typed the value they hold instead of the name of the variable.
To create a variable, you just provide a name and value for it. Your variable names should be descriptive and remind you of the value they hold. A variable name cannot start with a number, nor can it contain spaces. It can, however, start with an underscore. Apart from that, you can use any mix of upper- and lowercase alphanumeric characters. Variables Parameters are not exactly parameters, rather the combination of positional parameters and variables in shell script functions scope.
    - [ Variable argument example ](./2_variable_parameters.sh)

---

## Script Input 

Other question that arouses is: why do I need to know this ? Short answer: because these are basics of input and data passing in shell scripts.

Input is usually can be provided with `read` command:

[for example input with echo ](./3_input_with_echo.sh) or [another input with prompt](./4_input_with_prompt.sh)

Although it can have much use of `$1` positional parameter can also be implemented and then parse the value either as long string or a type of array

<div style="page-break-after: always; visibility: hidden"> </div>

---

## Practice


<div style="page-break-after: always; visibility: hidden"> </div>
