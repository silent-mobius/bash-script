#!/usr/bin/env bash

########################################################################
#Created by: Silent-Mobius
#Purpose: Exercise
#Date: 01.01.2020
#version: 1.0.0
set -o pipefail # should stop script when a | b, either a or b fails
set -o errexit # should stop script on any error
set -o noclobber # should not allow to over-write script
set -o nounset # do not allow un-used variables
########################################################################


# what about $0
: '
$0 is pointer to the 
name of the
'

echo '$0' usually is script name thats is called $0
echo it even recieved another one as "\$1=$1" and "\$2=$2"

echo The first argument is $1      # gettting 1st variable out side of script 
echo The second argument is $2     # gettting 2nd variable 
echo The third argument is $3	   # gettting 3rd variable

echo you can even use 15 positional parameters but you will have to encase them with '{}' e.g  '${15}'
# shift needs to be explained
# need to show how to work with bash built-in tools of text manipulation
