<center style="background-color:lightgreen">
    <h1 style="color:black"> 
     Bash: Data Arrays
    </h1>
</center>



## Arrays

An array is a variable containing multiple values. Any variable may be used as an array. There is no maximum limit to the size of an array, nor any requirement that member variables be indexed or assigned contiguously. Arrays are zero-based: the first element is indexed with the number 0.

Indirect declaration is done using the following syntax to declare a variable:
```sh
ARRAY[INDEXNR]=value
```
The INDEXNR is treated as an arithmetic expression that must evaluate to a positive number.

Explicit declaration of an array is done using the declare built-in:
```sh
declare -a ARRAYNAME
```
A declaration with an index number will also be accepted, but the index number will be ignored. Attributes to the array may be specified using the declare and readonly built-ins. Attributes apply to all variables in the array; you can't have mixed arrays.

Array variables may also be created using compound assignments in this format:
```sh
ARRAY=(value1 value2 ... valueN)
```
Each value is then in the form of [indexnumber=]string. The index number is optional. If it is supplied, that index is assigned to it; otherwise the index of the element assigned is the number of the last index that was assigned, plus one. This format is accepted by declare as well. If no index numbers are supplied, indexing starts at zero.

Adding missing or extra members in an array is done using the syntax:
```sh
ARRAYNAME[indexnumber]=value
```
Remember that the read built-in provides the -a option, which allows for reading and assigning values for member variables of an array.

---

### Defining arrays

In shell we can define 2 main types of arrays:
- Indexed array: use positive integer numbers as keys. Indexed arrays are always __sparse__, meaning indexes are not necessarily contiguous.
- Associative array: use arbitrary nonempty strings as keys, sometimes known as a "dict" in python or "hashmap" in ruby.

> Note:  Bash supports two different types of ksh-like one-dimensional arrays. __Multidimensional arrays are not implemented__. 

Here simple declaration example:

```sh
declare -a Hereos=() # Declares an indexed array Heroes. An existing array is not initialized
Heroes=('batman' 'superman' 'green-lantern') # Declares an indexed array Heroes and initializes it to be full of data.
                                             #  This can also be used to empty an existing array
Heroes[0]="superman" # Allows to access FIRST element that starts with zero, in array and override it with different data
Heroes[1]="batman"   # Same as above.
Heroes[2]="green-lantern" # Same as above, with small difference, we are keeping old value.
```

---

### Working with arrays

We can access array data by its indexed position or by its associative name given by us.

```sh
echo ${Heroes[0]}           # Element #0
echo ${Heroes[-1]}          # Last element
echo ${Heroes[@]}           # All elements, space-separated
echo ${#Heroes[@]}          # Number of elements
echo ${#Heroes}             # String length of the 1st element
echo ${#Heroes[3]}          # String length of the Nth element
echo ${Heroes[@]:3:2}       # Range (from position 3, length 2)
echo ${!Heroes[@]}          # Keys of all elements, space-separated
```

---

### Array Operations

We can also use operators on arrays to extend or remove data in/from them.

```sh

Heroes=("${Heroes[@]}" "flash")             # Push
Heroes+=('flash')                           # Also Push
Heroes=( ${Heroes[@]/Ap*/} )                # Remove by regex match
unset Heroes[2]                             # Remove one item
Heroes=("${Heroes[@]}")                     # Duplicate
Heroes=("${Heroes[@]}" "${Anti-Heroes[@]}") # Concatenate
lines=($(cat "logfile"))                    # Read from file
```

---

# Iteration

We can iterate through the array with out looping tools : for, while and until

```sh
data_array=(1 2 a b 4 c 55 66 dd ee )

for element in "${data_array[@]}"; 
  do
    echo $element
  done
```

```sh
data_array=( 1 2 a b 4 c 55 66 dd ee )
array_length=${#data_array[@]}
element=0

while [ $array_length -gt $element ]
  do
    echo  ${data_array[element]}
    (( element++ ))
  done
```

```sh
data_array=( 1 2 a b 4 c 55 66 dd ee )
array_length=${#data_array[@]}
element=0

until [ ! $array_length -gt $element ]
  do
    echo  ${data_array[element]}
    (( element++ ))
  done
```

---

# Associative array

## Defining

```sh
declare -A sounds

sounds[dog]="bark"
sounds[cow]="moo"
sounds[bird]="tweet"
sounds[wolf]="howl"
```


---

# Working with associative arrays

```sh
echo ${sounds[dog]} # Dog's sound
echo ${sounds[@]}   # All values
echo ${!sounds[@]}  # All keys
echo ${#sounds[@]}  # Number of elements
unset sounds[dog]   # Delete dog
```
---
# Iteration

Iterate over values
```sh
for val in "${sounds[@]}";
do
  echo $val
done
```
Iterate over keys
```sh
for key in "${!sounds[@]}"; 
do
  echo $key
done

```

<div style="page-break-after: always; visibility: hidden"> </div>

---

### Practice

- Write a script that does the following:
  - Display the name of the script being executed
  - Display the first, third and tenth argument given to the script.
  - Display the total number of arguments passed to the script.
  - If there were more than three positional parameters, use shift to move all the values 3 places to the left.
  - Print all the values of the remaining arguments.
  - Print the number of arguments.
Test with zero, one, three and over ten arguments.

<div style="page-break-after: always; visibility: hidden"> </div>
