#!/usr/bin/env bash

########################################################################
#Created by: Silent-Mobius
#Purpose: Exercise
#Date: 01.01.2020
#version: 1.0.0
set -o pipefail # should stop script when a | b, either a or b fails
set -o errexit # should stop script on any error
set -o noclobber # should not allow to over-write script
set -o nounset # do not allow un-used variables
########################################################################


declare -a packages=(vi vim gvim ninja-ide guake gcc )

function package_install(){
	OPT=$1
	packCheck=$(dpkg -l |grep -v grep|grep "$OPT" &> /dev/null;echo $?)
	if [ "$packCheck" == "0" ];then
		true
	else
		apt-get install  -y "$OPT"
	fi
}

if [ "$EUID" == "0" ];then
for package in "${packages[@]}";
	do
		package_install "$package"
	done
else
	echo "PLease get Root Privileges"
fi
