#!/usr/bin/env bash

########################################################################
#Created by: Silent-Mobius
#Purpose: Exercise
#Date: 01.01.2020
#version: 1.0.0
set -o pipefail # should stop script when a | b, either a or b fails
set -o errexit # should stop script on any error
set -o noclobber # should not allow to over-write script
set -o nounset # do not allow un-used variables
########################################################################


echo '
Fruits=( "apple" "banana" "orange")
'

Fruits=( "apple" "banana" "orange")

echo "declaring  array in different way:"

echo "Fruits[0]='apple'
	  Fruits[1]='banana'
	  Fruits[2]='orange'"

Fruits[0]='apple'
Fruits[1]='banana'
Fruits[2]='orange'
Fruits[3]='papaya'

echo "${Fruits[@]}"