#!/usr/bin/env bash

########################################################################
#Created by: Silent-Mobius
#Purpose: Exercise
#Date: 01.01.2020
#version: 1.0.0
set -o pipefail # should stop script when a | b, either a or b fails
set -o errexit # should stop script on any error
set -o noclobber # should not allow to over-write script
set -o nounset # do not allow un-used variables
########################################################################


Fruits=(apple watermelon melon orange banana papaya)

echo "this is a list of Fruits: ${Fruits[@]}"

echo "removing the whole arraya";sleep 2

unset Fruits							# Remove whole arrya

echo "this is an updated list of Fruits: ${Fruits[@]}"
