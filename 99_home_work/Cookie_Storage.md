# Create new repo at github called: Cookie_Storage

## Create a script that will perform several functions:

- function that will check  and list all possible  block devices.(do not use commands for device listing)
- function that will change filesystem according to parameters provided by users request.(use select loop to choose a type of filesystem: ext3/etx4/ntfs/jfs.. so on and so forth)
- function that will change partition as requested by user.(pass the values to parted/fdisk)


---

Copy right © Alex M. Schapelle, VAIOLabs LTD.  vaiolabs.io License: GPLv3