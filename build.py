#!/usr/bin/env python3
###########################################################
# Created: by Silent-Mobius
# purpose: to generate pdf file from all the markdown files
# version: 0.6.10
##########################################################

import os
import sys
import time
import glob
try:
    import shutil
    import pdfkit
    import pygments
    import markdown
    from markdown.extensions.codehilite import CodeHiliteExtension
    from markdown.extensions.fenced_code import FencedCodeExtension
    from markdown.extensions.extra import ExtraExtension
    from pygments import highlight
    from pygments.formatters import HtmlFormatter
    from pygments.lexers import get_lexer_by_name, TextLexer

except ImportError as e:
    print('[!] Dependencies missing: {0}'.format(e)+'\n'+'[+] Please verify that {0} lib is installed'.format(e))
    sys.exit(1)

TILDA=os.getcwd()
HEAD='''
<!DOCTYPE html>
<head>
  <link rel="stylesheet" href="styles.css"> 
</head>
'''
s='---------------------------'


def main():
    if not os.path.exists('out'):
        os.mkdir('out')
        os.symlink(TILDA+'/99_misc/', TILDA+'/out/99_misc')
    clean_up()
    print(f'{s}\n [+] Getting path to files \n{s}')
    clear()
    path_list = get_path_list_for_readme_files()
    print(f'{s}\n [+] Concatenating files in to one \n{s}')
    build = concatenate_all_readme_files(path_list)
    header = scan_all_readme_file_headers(path_list)
    clear()
    if build:
        print(f'{s}\n [+] Converting to HTML file \n{s}')
        convert_concatenated_file_to_html('out/build.md')
        clear()
        print(f'{s}\n [+] Converting to PDF \n{s}')
        convert_html_file_to_pdf('out/presentation.html')
        clear()
    else:
        print(f'{s}\n [!] Error occurred: Please notify the developer \n{s}')


def clear():
    time.sleep(2)
    os.system('clear')


def get_path_list_for_readme_files(folder_path='.'):
    exclude_prefixes=('.git', '.vscode', '99_misc', '99_home_work', 'Exercises', 'built_in_commands', 'out')
    project_root = os.path.join(os.getcwd())
    directory_path_list = []
    try:
        for root, directories, files in os.walk(project_root,topdown=True):
            if not '.git' in root.split('/') \
            and (not '.vscode' in root.split('/')) \
            and (not 'built_in_commands' in root.split('/')) \
            and (not '99_misc' in root.split('/')) \
            and (not '99_home_work' in root.split('/')) \
            and (not 'out' in root.split('/'))  \
            and (not 'Exercises' in root.split('/')):
                directory_path_list.append(root) # this needs to be implemented with exclude_prefixes
    except:
        pass # at the moment exceptions do not matter
    directory_path_list.sort()
    return  directory_path_list


def concatenate_all_readme_files(path_list, temp_build_file='out/build.md'):
    if len(path_list):
        for path in path_list:
            # print(path)
            path = str(path)+'/README.md'
            # print(path)
            with open(temp_build_file,'a') as build_file_content:
                with open(path, 'r') as content:
                    build_file_content.write(content.read())
        return True
    else:
        return False


def scan_all_readme_file_headers(path_list, temp_build_file='out/build_headers.md'):
    init = 0
    header_dict={}
    if len(path_list):
        for path in path_list:
            path = str(path)+'/README.md'
            with open(temp_build_file,'a') as header_file:
                with open(path,'r') as content:
                    data = content.readlines()
                for line in data:
                    # print(line)
                    if line.startswith('#'):
                        init += 1
                        if init not in header_dict:
                            header_dict[init]=line
                # print(header_dict)
                for k,v in header_dict.items():
                    header_file.write(f'{k}   .............   {v}')
        return True
    else:
        return False



def convert_concatenated_file_to_html(file_name):
    hi_lite = CodeHiliteExtension(use_pygments=True, css_class='monokai', linenums=False, guess_lang=True )
    fence = FencedCodeExtension()
    extras = ExtraExtension()
    os.system('pygmentize -S monokai -f html -a .monokai > out/styles.css')
    with open('out/presentation.html', 'a') as html:
        with open(file_name) as markdown_file:
            converted_content = markdown.markdown(markdown_file.read(), extensions=['nl2br', fence, hi_lite, extras])
        html.write(HEAD)
        html.write(converted_content)


def convert_html_file_to_pdf(file_name):
    WKHTMLTOPDF_PATH = '/usr/bin/wkhtmltopdf' # need to make this dynamic
    config = pdfkit.configuration(wkhtmltopdf=WKHTMLTOPDF_PATH)
    options = {'page-size': 'A4','margin-top': '10','margin-right': '15',
            'margin-left': '15','margin-bottom': '10','zoom': '1.0','encoding': "UTF-8", "enable-local-file-access": None}
    pdfkit.from_file(file_name, 'book.pdf',configuration=config, options=options)


def clean_up():
    print(f'{s}\n [+] Checking previous builds  \n{s}')
    for file in ['out/build.md','out/presentation.html','out/book.pdf', 'out/build_headers.md', 'book.pdf']:
        if os.path.exists(file):
            os.remove(file)
    # os.system('clear')        
    print(f'{s}\n [+] Clean Up Successful \n{s}')
    clear()



if __name__ == "__main__":
    main()