#!/usr/bin/env bash

########################################################################
#Created by: Silent-Mobius
#Purpose: Exercise
#Date: 01.03.2021
#version: 1.0.0
########################################################################


echo "5 + 27 = `expr 5 + 27`"
echo "35 - 15 = `expr 35 - 15`"
echo "n=150"; n=150; sleep 2
echo "expr $n*7="$(expr $n * 7)
sleep 2
echo "expr $n-7=" $(expr $n - 7)
sleep 2
echo "expr $n/7="$(expr $n / 7)
sleep 2
echo "expr $n+7=" $(expr $n + 7)

echo $((++n+10)) # pre-incriment
echo $((n++))    # post-incriment
echo $((--n-10)) # pre-reduce
echo $((n--))    # post-reduce
