
---

<center style="background-color:lightgreen">
    <h1 style="color:black"> 
    Expansions and substitutions
    </h1>
</center>

This chapter introduces you to shell expansion by taking a close look at built-ins, commands and arguments. Shell expansion  and substitution are important subject to be acknowledge , due to verity of commands on our Linux systems are processed and most likely changed by the shell before they are executed.
The command line interface (CLI) or shell which are used on most Linux distribution is bash, which stands for *Bourne again shell*. Bash shell incorporates features from the original Bourne shell (*sh*), the C shell (*csh*), The exTended C Shell (*tcsh*) and the Korn shell (*ksh*).
We will focus during this chapter and this book mostly on Bash, yet for historical and enriching reasons we will mention other shells as well

---

### Introduction to expansions and substitutions

Type | Explanation
--- | ----
{A,B,C} {A..C} | Brace expansion
~/ ~/.config | Tilde expansion
$FOO ${BAR%.mp3} | Parameter expansion
*command* $(command) |  Command substitution
<(command) >(command) | Process substitution
$((1 + 2 + 3)) $[4 + 5 + 6] | Arithmetic expansion
Hello <---> Word! | Word splitting
/data/*-av/*.mp? |Pathname expansion

In the table above we have list of possible expansions that we are most capable to use within the Bash shell or in various other shells such as ksh or tcsh.

---

## Compound commands overview

### Grouping and Conditionals

- *{ …; }*  command grouping
- *( … )*   command grouping in a sub-shell
- *[ ... ]*  | older conditional expression => suggested NOT to use
- *(( ... ))* |  arithmetic evaluation
- *select word in …; do …; done*  |  user selections

---

### Control flow and data processing

Commands that operate on data and/or affect control flow.

Command | Alt | Type
--- | ---- | ----
*colon* | "true" null command.    true |  special builtin
*dot* |   Source external files.  source  special builtin
*false* | Fail at doing nothing.  |   builtin
*continue* / break |  continue with or break out of loops. | special builtin
*let* |   Arithmetic evaluation simple command. | builtin
*return* |    Return from a function with a specified exit status. | special builtin
*[[* | The extension of classic test command. |  builtin

---

### Configuration and Debugging

Commands that modify shell behavior, change special options, assist in debugging.

Command | Alt | Type
---- | ---- | ----
*caller*  |Identify/print execution frames. | builtin
*set* | Set the positional parameters and/or set options that affect shell behavior  |   special builtin
*shopt* | set/get some bash-specific shell options | builtin  

---

### I/O

Commands for reading/parsing input, or producing/formatting output of standard streams.

Command | Alt | Type
--- | ---- | ---
*coproc* |    Co-processes: Run a compound command in the background with async I/O.  |   keyword
*echo* |  Create output from arguments.   |   builtin
*mapfile* |   Read lines of input into an array.  readarray | builtin
*printf* |    "advanced echo." |  builtin
*read* |  Read input into variables or arrays, or split strings into fields using delimiters. |   builtin

---

### Process and Job control

Commands related to jobs, signals, process groups, sub-shells.

Command |   Alt |   Type
---- | ---- | ---
*exec* |  Replace the current shell process or set redirection.  |   special builtin
*exit* |  Exit the shell.|    special builtin
*kill* |  Send a signal to specified process(es)  |   builtin
*trap* |  Set signal handlers or output the current handlers. |   special builtin
*times* | Display process times. |    special builtin
*wait* |  Wait for background jobs and asynchronous lists.    | builtin

---

### Declaration commands

Commands that set and query attributes/types, and manipulate simple data-structures. 

Command | Alt | Type
--- | --- | ---
*declare* |   Display or set shell variables or functions along with attributes.  typeset |   builtin
*export* |    Display or set shell variables, also giving them the export attribute.  typeset -x |    special builtin
*eval* |  Evaluate arguments as shell code.   |   special builtin
*local* | Declare variables as having function local scope.   | builtin
*readonly* |  Mark variables or functions as read-only.   typeset -r |    special builtin
*unset* | Unset variables and functions.  |   special builtin
*shift* | Shift positional parameters |special builtin

---

## `Set` options

Set or unset values of shell options and positional parameters. Change the value of shell attributes and positional parameters, or display the names and values of shell variables.

- *set -o noclobber* : Avoid overlay files (echo "hi" > foo)
- *set -o errexit*   : Used to exit upon error, avoiding cascading errors
- *set -o pipefail*  : Unveils hidden failures
- *set -o nounset*   : Exposes unset variables

--- 

### Glob options

We can change the setting of each shell option OPTNAME listed below. Without any option arguments, list each supplied OPTNAME, or all shell options if no OPTNAMEs are given, with an indication of whether or not each is set.

- *shopt -s nullglob*    : Non-matching globs are removed  ('*.foo' => '')
- *shopt -s failglob*    : Non-matching globs throw errors
- *shopt -s nocaseglob*  : Case insensitive globs
- *shopt -s dotglob*     : Wildcards match dot files ("*.sh" => ".foo.sh")
- *shopt -s globstar*    : Allow ** for recursive matches ('lib/**/*.rb' => 'lib/a/b/c.rb')

<div style="page-break-after: always; visibility: hidden"> </div>


---

<center style="background-color:lightgreen">
    <h1 style="color:black"> 
     Operators and data processing
    </h1>
</center>


In any [programming language](https://en.wikipedia.org/wiki/Programming_language), there is set of instructions that help the management content or information.(that is also called `data`) those instructions are called [operators](https://en.wikipedia.org/wiki/Operator_(computer_programming)), and while there are many types of then, the most basic `operators` are taken from vastly known fields, such as:

- Mathematics
- Philosophy
- Electronics


While we speak about operators, it is important to mention that, we mainly focus on [bourne again shell: Bash](https://en.wikipedia.org/wiki/Bash_%28Unix_shell%29), and although all operators are available, but Bash's capability of complex calculations is some what limited, for example, calculation decimal fraction is not always possible, can be seen in this [example](./0_fraction_error.sh). The tools that help us to do those calculations are commands such as:

- `expr`
- `let`
- `bc`
- External scripting shells such as `perl`/`python`/`ruby`/`nodejs`

### Operation Table

Below is several tables of operator types, with links to specific examples.

##### Arithmetic operators

Arithmetic expansion allows the evaluation of an arithmetic expression and the substitution of the result. The format for arithmetic expansion is:
```sh
#!/bin/bash

$(( EXPRESSION ))
```
The expression is treated as if it were within double quotes, but a double quote inside the parentheses is not treated specially. All tokens in the expression undergo parameter expansion, command substitution, and quote removal. Arithmetic substitutions may be nested.

Evaluation of arithmetic expressions is done in fixed-width integers with no check for overflow - although division by zero is trapped and recognized as an error. The operators are roughly the same as in the C programming language. In order of decreasing precedence, the list looks like this:

Operators | Description
--------- | -----------
++ --     |    Auto-increment and auto-decrement integer, both prefix and postfix
\* / %    |    Multiplication, division, modulus (remainder) of integer
\+ \-     |    Addition, subtraction of integer
\*\*      |    Exponent of integer

[see arithmetic example](./1_arithmetic.sh)


##### Assignment and Comparison operators

Operators | Description
--------- | -----------
=         |    Assignment of variable to value
== !=     |    Equality, inequality (both evaluated left to right) of **string**
+=        |    Increment value of variable and store in the same variable (integer type)
-=        |    Reduce value of variable and store in the same variable  (integer type)
*=        |    Multiply value of variable and store in the same variable  (integer type)
/=        |    Divide value of variable and store in the same variable  (integer type)
%=        |    Modulus value of variable and store in the same variable  (integer type)
-eq       |    Equal of integer
-ne       |    Not Equal of integer
-gt       |    Greater Then of integer
-lt       |    Less Then of integer
-ge       |    Greater or Equal of integer
-le       |    Less or Equal of integer

> Note: there are also bitwise operators that we do NOT dive in deeply, and just mention them in this chapter.

[see assignment example](./2_assignment.sh)
[see comparison example](./3_comparisons.sh)

---


##### Logical and Bitwise operators

Operators | Description
--------- | -----------
&&        |      Logical AND 
\|\|      |      Logical OR 
\!        |      Logical NOT
<< >>     |      Bitwise left shift, bitwise right shift
< <= > >= |      Less than, less than or equal to, greater than, greater than or equal to
&         |      Bitwise AND
^         |      Bitwise exclusive OR
\|        |      Bitwise OR
?:        |      Inline conditional evaluation

> Note: Because  let  and  ((...))  are  built  in  to  the  shell,  they  have access to variable values. It is not necessary to precede a variable’s name with a dollar sign in order to retrieve its value (doing so does work, of course).The exit status of let is confusing. It’s zero (success) for a non-zero  mathematical  result,  and  non-zero  (failure)  for  a  zero mathematical result.

[see logical example](./4_logical.sh)
[see bitwise example](./5_bitwise.sh)

---

##### File operators


Operators | Description
--------- | -----------
 `-d`     | exists and is directory
 `-e`     | file exists and is regular
 `-b`/`-c`/`-S`/`-p` | file exists and is block type or character type or socket type or named-pipe type accordingly
 `-f`/`-r`/`-w`/`-x` | file exists and is regular or is readable or is writable or is executable accordingly 
 `-L` | file exists and is symbolic link


 [see file operators example](./6_fileperator.sh)


---

# Practice

- Create a shell script:
    - Assign 2 variable of var_x and var_y with numeric values
    - Use all the arithmetic and logical operators
    - Reassign to variable var_x and var_y with string values
    - Use all arithmetic operators
    - Use variable expansions

<div style="page-break-after: always; visibility: hidden"> </div>

---

<center style="background-color:lightgreen">
    <h1 style="color:black"> 
    Expansions and substitutions
    </h1>
</center>
