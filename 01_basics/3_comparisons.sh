#!/usr/bin/env bash

########################################################################
#Created by: Silent-Mobius
#Purpose: Exercise
#Date: 01.03.2021
#version: 1.0.0
########################################################################

n=50
if [ $n -eq 80 ]
then
  echo "The number is equal to 80"
else
  echo "The number is not equal to 80"
fi

n=50
if [ $n -ne 100 ]
then
  echo "The number is not equal to 100"
else
  echo "The number is equal to 100"
fi

n=50
if [ $n -gt 50 ]
then
  echo "The number is greater than 50 "
else
  echo "The number is less than or equal to 50"
fi

n=50
if [ $n -ge 50 ]
then
  echo "The number is greater than or equal to 50"
else
  echo "The number is less than 50"
fi

n=50
if [ $n -lt 50 ]
then
  echo "The number is less than 50"
else
  echo "The number is greater than or equal to 50"
fi

n=50
if [ $n -le 50 ]
then
  echo "The number is less than or equal to 50"
else
  echo "The number is greater than 50"
fi

n=50
if [[ $n < 50 ]]
then
  echo "The number is less than 50"
else
  echo "The number is greater than or equal to 50"
fi

n=55
if (( $n <= 50 ))
then
  echo "The number is less than or equal to 50"
else
  echo "The number is greater than 50"
fi

n=55
if (( $n > 50 ))
then
  echo "The number is greater than 50"
else
  echo "The number is less than or equal to 50"
fi

n=55
if (( $n >= 55 ))
then
  echo "The number is greater than or equal to 55"
else
  echo "The number is less than 55"
fi


str="Mango"
if [ $str = "Orange" ]
then
  echo "The value are equal"
else
  echo "The value are not equal"
fi

var=100
if [ $var == 100 ]
then
  echo "The value is equal to 100"
else
  echo "The value is not equal to 100"
fi

var=50
if [ $var != 100 ]
then
  echo "The value is not equal to 100"
else
  echo "The value is equal to 100"
fi


str1="Mango"
str2="Orange"
if [[ $str < $str2 ]]
then
  echo "$str1 is lower than $str2"
else
  echo "$str1 is greater than $str2"
fi

str1="Mango"
str2="Orange"
if [[ $str > $str2 ]]
then
  echo "$str1 is greater than $str2"
else
  echo "$str2 is greater than $str1"
fi
