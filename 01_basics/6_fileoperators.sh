#!/usr/bin/env bash

########################################################################
#Created by: Silent-Mobius
#Purpose: Exercise
#Date: 01.03.2021
#version: 1.0.0
########################################################################


filename=/etc/passwd
if [ -e $filename ]
then
  echo "File or Folder exists."
else
  echo "File or Folder does not exist."
fi

if [ -f "/etc/passwd" ]
then
  echo "File exists."
else
  echo "File does not exist."
fi

filename=/etc/passwd
if [ -s $filename ]
then
  echo "File size is more than zero."
else
  echo "File size is zero."
fi

name=/home
if [ -d $name ]
then
  echo "Folder exists."
else
  echo "Folder does not exist."
fi

name=/dev/sda
if [ -b $name ]
then
  echo "This is a block special file."
else
  echo "This is not a block special file."
fi

name=/dev/tty0
if [ -c $name ]
then
  echo "This is a character special file."
else
  echo "This is not a character special file."
fi


pipe_test()
{
[ -p /dev/fd/0 ] && echo "File is a pipe" || echo "File is not a pipe"
}
echo "Hello" | pipe_test

name=/dev/rtc
if [ -h $name ]
then
  echo "It is a symbolic link."
else
  echo "It is not a symbolic link."
fi

name=/dev/rtc
if [ -S $name ]
then
  echo "It is a socket."
else
  echo "It is not a socket."
fi

name=$0
if [ -x $name ]
then
  echo "File has execution permission."
else
  echo "File does not have execution permission."
fi

name=sudo
if [ -g $name ]
then
  echo "Group id is set."
else
  echo "Group id is not set."
fi


if [ -u $1 ]
then
  echo "User id is set."
else
  echo "User id is not set."
fi
