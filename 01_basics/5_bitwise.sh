#!/usr/bin/env bash

########################################################################
#Created by: Silent-Mobius
#Purpose: Exercise
#Date: 01.03.2021
#version: 1.0.0
########################################################################

echo $((3 & 6))
var=3
echo "var value is: (($var&=7))"
echo $((3 | 6))

var=4
echo "var value is:((var|=2))"
echo $var

echo $((3 ^ 6))

var=5
echo "var value is:((var^=2))"
echo $var

echo $(( ~7 ))

echo $(( 6<<1 ))

var=5
echo "var value is:((var <<= 1))"
echo $var

echo $(( 8>>1 ))

var=7
echo "var value is:((var >>= 1))"
echo $var
