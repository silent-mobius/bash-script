#!/usr/bin/env bash

########################################################################
#Created by: Silent-Mobius
#Purpose: Exercise
#Date: 01.03.2021
#version: 1.0.0
########################################################################

echo "Here is example of fraction errors:"
echo  "5 + 7 = $(( 5 + 7 ))"
echo  "5.2 + 7.1 = $(( 5.2 + 7.1 ))"
echo  "4 * 2 = $(( 4 * 2 ))"
echo  "4 / 2 = $(( 4 / 2 ))"
echo  "4.8 / 2.2 = $(( 4.8 / 2.2 ))"
