#!/usr/bin/env bash

########################################################################
#Created by: Silent-Mobius
#Purpose: Exercise
#Date: 01.03.2021
#version: 1.0.0
########################################################################

# you need to pass argument values to script before running it.

if [[ $1 = "fahmida" && $2 = "abcd" ]]
then
  echo "Valid user"
else
  echo "Invalid user"
fi

if [[ $1 = 101 || $1 = 780 ]]
then
  echo "You have won the ticket"
else
  echo "Try again"
fi

terminate=$1
if [[ !$terminate  ]]
then
  echo "Program is running"
else
  echo "Program is terminated"
fi
