<center style="background-color:lightgreen">
    <h1 style="color:black"> 
     Bash: Scripting Functions
    </h1>
</center>

## Functions

Shell functions are a way to group commands for later execution, using a single name for this group, or routine. The name of the routine must be unique within the shell or script. All the commands that make up a function are executed like regular commands. When calling on a function as a simple command name, the list of commands associated with that function name is executed. A function is executed within the shell in which it has been declared: no new process is created to interpret the commands.

Special built-in commands are found before shell functions during command lookup. The special built-ins are: `break`, `:`, `.`, `continue`, `eval`, `exec`, `exit`, `export`, `readonly`, `return`, `set`, `shift`, `trap` and `unset`.

#### Defining Bash Functions

The syntax for declaring a bash function is straightforward. Functions may be declared in two different formats:

    The first format starts with the function name, followed by parentheses. This is the preferred and more used format.
```sh
function_name () {
      commands
    }
```

The second format starts with the reserved word function, followed by the function name.
```sh
function function_name {
  commands
}
```

#### Few points to be noted:
  - The commands between the curly braces {} are called the body of the function. The curly braces must be separated from the body by spaces or newlines.
  - Defining a function doesn’t execute it. To invoke a bash function, simply use the function name. Commands between the curly braces are executed whenever the function is called in the shell script.
  - The function definition must be placed before any calls to the function.
  - When using single line “compacted” functions, a semicolon ; must follow the last command in the function.
  - Always try to keep your function names descriptive.

---

#### Passing Arguments to Bash Functions

To pass any number of arguments to the bash function simply put them right after the function’s name, separated by a space. It is a good practice to double-quote the arguments to avoid the mis-parsing of an argument with spaces in it.

- The passed parameters are $1, $2, $3 … $n, corresponding to the position of the parameter after the function’s name.
- The $0 variable is reserved for the function’s name.
- The $# variable holds the number of positional parameters/arguments passed to the function.
- The $* and $@ variables hold all positional parameters/arguments passed to the function.
      - When double-quoted, "$*" expands to a single string separated by space (the first character of IFS) - "$1 $2 $n".
      - When double-quoted, "$@" expands to separate strings - "$1" "$2" "$n".
      - When not double-quoted, $* and $@ are the same.



### Raising errors

Unlike functions in most programming languages, Bash functions don’t allow you to return a value when called. When a bash function completes, its return value is the status of the last statement executed in the function, 0 for success and non-zero decimal number in the 1 - 255 range for failure.

The return status can be specified by using the return keyword, and it is assigned to the variable $?. The return statement terminates the function. You can think of it as the function’s exit status .

```sh
myfunc() {
  return 1
}

if myfunc; then
  echo "success"
else
  echo "failure"
fi
```

Another, better option to return a value from a function is to send the value to stdout using echo or printf like shown below:
```sh
#!/usr/bin/env bash

my_function () {
  local func_result="some result"
  echo "$func_result"
}

func_result="$(my_function)"
echo $func_result
```
Instead of simply executing the function which will print the message to stdout, we are assigning the function output to the func_result variable using the $() command substitution. The variable can later be used as needed.
---

### Arguments

- `$#` :	Number of arguments
- `$*` :	All positional arguments (as a single word)
- `$@` :	All positional arguments (as separate strings)
- `$1` :	First argument
- `$_` :	Last argument of the previous command

> Note: $@ and $* must be quoted in order to perform as described. Otherwise, they do exactly the same thing (arguments as separate strings).


<div style="page-break-after: always; visibility: hidden"> </div>
