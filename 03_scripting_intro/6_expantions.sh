#!/usr/bin/env bash

########################################################################
#Created by: Silent-Mobius
#Purpose: Exercise
#Date: 01.03.2021
#version: 1.0.0
set -o pipefail # should stop script when a | b, either a or b fails
set -o errexit # should stop script on any error
set -o noclobber # should not allow to over-write script
set -o nounset # do not allow un-used variables
########################################################################

read -p "Please provide name: " name

echo Name $name was provided

echo We expand it with '${}', thus use '${name}' print ${name} yet the output will be the same
sleep 2
echo  The output can be manipulated with '${name^}' to capitilize the variable : ${name^}
sleep 2 
echo You can also capitilize all letters with '${name^^}' : ${name^^}
sleep 2
echo We can overide the variable name as follow 'name=${name^^}' : name=${name^^}
sleep 2
echo  The minimize first letter with  '${name,}' to capitilize the variable : ${name,}
sleep 2 
echo Or minimize all letters with  '${name,,}' : ${name,,}
sleep 2
echo We can count the length of variable with '${#name}':we\'ll get number of characters of ${#name}
sleep 2
echo We can get index from the variable with '${name:2}': we\'ll get letter on the position 2: ${name:2}
sleep 2 
echo Index can be negative providing letter from the end with '${name:(-1)}' : ${name:(-1)}