#!/usr/bin/env bash

########################################################################
#Created by: Silent-Mobius
#Purpose: Exercise
#Date: 01.03.2021
#version: 1.0.0
set -o pipefail # should stop script when a | b, either a or b fails
set -o errexit # should stop script on any error
set -o noclobber # should not allow to over-write script
set -o nounset # do not allow un-used variables
########################################################################

script_var="var"
local data="ninja"
declare -a array=(e0 e1 e2 e3 e4 e5 e6)

echo $script_var

echo $data

echo "${array[@]}"

