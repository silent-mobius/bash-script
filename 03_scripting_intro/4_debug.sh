#!/usr/bin/env bash

########################################################################
#Created by: Silent-Mobius
#Purpose: Exercise
#Date: 01.01.2020
#version: 1.0.0
########################################################################

# Every script should have safe header for development
set -x #enbaling internal debugging till `set +x` is met or the end of the script

var=42 #setting variable

echo "What is the meaning of life?"
sleep 2
echo "The answer is $var"
sleep 2
echo -e "Plase read/watch Hitchhikers guide through the galaxy \n and visit: shorturl.at/egh28"