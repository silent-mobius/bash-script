
---

<center style="background-color:lightgreen">
    <h1 style="color:black"> 
        Intro to Bash Scripting/Programming
    </h1>
</center>


## Just for starters

It is mandatory to understand core of concents, we can communicate in language that has same basis behind it. Rational to this is that we speak different language and words tend to change their original meaning, and to avoid it we either purge our minds, which is only science fiction or we clear doubts of word definitions. Thus before diving in to  the book , please search the world wide web or wikipedia for the list of questions below:

- [What is programming language ?](https://en.wikipedia.org/wiki/Programming_language)
- [What is scripting language?](https://en.wikipedia.org/wiki/Scripting_language)
- [What is POSIX?](https://en.wikipedia.org/wiki/POSIX)
- [What is POSIX according to stack-overflow ?](https://stackoverflow.com/questions/1780599/what-is-the-meaning-of-posix)
- [What is bash shell scripting in particular ?](https://en.wikipedia.org/wiki/Bash_%28Unix_shell%29)
- [What is variable ?](https://en.wikipedia.org/wiki/Variable_(computer_science))
- [What is variable type?](https://en.wikipedia.org/wiki/Data_type)
- [What is environment variable?](https://en.wikipedia.org/wiki/Environment_variable)
- [What is function ?](https://en.wikipedia.org/wiki/Function_(computer_programming))

---

## Scripts

According to wikipedia: 
*"Bash is a command processor that typically runs in a text window where the user types commands that cause actions. Bash can also read and execute commands from a file, called a shell script"*

It means that `Bash script` is just file that has commands inside of it as shown below:

```sh 
ls 
pwd
cd /tmp
touch $HOME/this_is_my_file
rm $HOME/this_is_my_file`
echo done
```
Although it is true and it shall work in most cases, scripts still have way more in them then just commands. for example we can use of variables, which we will dive into later, as well as condition checks and iteration loops. When writing `Bash scripts`, it is always a good practice to make your code clean and easily understandable. Organizing your code in blocks, indenting, and giving descriptive names to variables and functions.

Here are some examples and initial things that we need to start with in order to create shell scripts

- [No style what so ever in this script](./0_style.sh)
- [Simple variable script that has variable](./1_variable_simple.sh)

One thing that needs to be emphasized is the manner that `Bash script` are executed:
- By providing execution permissions and giving path to the script for execution
```sh
chmod +x script.sh
/path/to/script.sh # relative path should also work
                  # this might fail if the commands are not based with executing shell
```
- Invoking the `bash` binary with the path of the script
```sh
bash /path/to/script.sh # this enforces the use of bash shell
```

---

## Comments

In addition to indents and blocks, another way to improve the readability of your code is by using *comments*. A *comment* is a human-readable explanation or annotation that is written in the shell script.

Adding comments to your `Bash scripts` will save you a lot of time and effort when you look at your code in the future, yet there is also chance that you will be confused with your own comments, thus take this with grain of salt: 

- Use short comments only when absolutely needed
- Be as precise as possible 
- Do not trust that your comments will be understood by others
- Remove comments once the issue is resolved or task has been done

Comments can be added at the beginning on the line or inline with other code:

```sh
# this is one line comment
echo test  # everything after pound is not address

```

Another option is to use HereDoc . It is a type of redirection that allows you to pass multiple lines of input to a command. If the HereDoc block is not redirected to a command, it can serve as a multiline comments placeholder:

```sh

<< comment 
multi line 
comment

: '
another way to multi-line comment
'
```
The last and special pound sign that we need to consider is known as shebang that looks something like this: 
```sh
#!/bin/bash
```
When a text file with a shebang is used as if it is an executable in a Unix-like operating system, the program loader mechanism parses the rest of the file's initial line as an interpreter directive. The loader executes the specified interpreter program, passing to it as an argument, to the path that was initially used when attempting to run the script, so that the program may use the file as input data.
The form of a shebang interpreter directive is as follows:
```sh
#!interpreter [optional-arg]
```
in which `interpreter` is generally an absolute path to an executable program. The optional argument is a string representing a single argument. White space after `#!` is optional.

In Linux, the file specified by `interpreter` can be executed if it has one of the following:
- The execute right and contains code which the kernel can execute directly
- A wrapper defined for it via sysctl (such as for executing Microsoft .exe binaries using wine)
- a shebang.

Keep in mind that shebang header may come in various manners such as:

```sh
#!/bin/sh              # this is old Bourne Shell
#!/bin/bash            # this used to be the default ans is still widely used
#!/usr/bin/bash        # in some UNIX like OSs this is the way to access bash
#!/usr/bin/env bash    # this is new way to access bash `env`  or `printenv` command
```

[Here are some examples of comments and shebang](./3_comments.sh)

---

## Easier to seek error and such

When things don't go according to plan, you need to determine what exactly causes the script to fail. Bash provides extensive debugging features. The most common is to start up the subshell with the `-x` option, which will run the entire script in debug mode. Traces of each command *plus* its arguments are printed to standard output after the commands have been expanded but before they are executed.
For example:
```sh
alex@vaiolabs.io ~ $ bash -x tmp.sh
+ var=42
+ echo 'What is the meaning of life?'
What is the meaning of life?
+ sleep 2
+ echo 'The answer is 42'
The answer is 42
+ sleep 2
+ echo -e 'Please read/watch Hitchhikers guide through the galaxy \n and visit: shorturl.at/egh28'
Please read/watch Hitchhikers guide through the galaxy
 and visit: shorturl.at/egh28
```
Or we can use `set -x` inside the script which will provide the same output

```sh
alex@vaiolabs.io ~ $ bash tmp.sh
+ set -x
+ var=42
+ echo 'What is the meaning of life?'
What is the meaning of life?
+ sleep 2
+ echo 'The answer is 42'
The answer is 42
+ sleep 2
+ echo -e 'Please read/watch Hitchhikers guide through the galaxy \n and visit: shorturl.at/egh28'
Please read/watch Hitchhikers guide through the galaxy
 and visit: shorturl.at/egh28
```
Whats the point then ? 
Using the set `Bash` built-in you can run in normal mode those portions of the script of which you are sure they are without fault, and display debugging information only for troublesome zones. Say we are not sure what the some command will do in the example script, then we could enclose it in the script like this:
```sh
set -x			# activate debugging from here
some command
set +x			# stop debugging from here
```

[Debugging your script](./4_debug.sh)

---

## Variables

In computer programming, a variable is a storage location (identified by a memory address) paired with an associated symbolic name, which contains some known or unknown quantity of information referred to as a value. The variable name is the usual way to reference the stored value, in addition to referring to the variable itself, depending on the context. This separation of name and content allows the name to be used independently of the exact information it represents. The identifier in computer source code can be bound to a value during run time, and the value of the variable may thus change during the course of program execution.

Variables in programming may not directly correspond to the concept of variables in mathematics. The latter is abstract, having no reference to a physical object such as storage location. The value of a computing variable is not necessarily part of an equation or formula as in mathematics. Variables in computer programming are frequently given long names to make them relatively descriptive of their use. [Taken from wikipedia](https://en.wikipedia.org/wiki/Variable_(computer_science))


#### Variable types

In terms of the classifications of variables, we can classify variables based on the lifetime of them. The different types of variables are:
- **static**: A static variable is also known as global variable, it is bound to a memory cell before execution begins and remains to the same memory cell until termination.  A typical example is the static variables in Bash, Perl, C and C++.
- **stack-dynamic**: A Stack-dynamic variable is known as local variable, which is bound when the declaration statement is executed, and it is reallocated when the procedure returns. The main examples are local variables in C subprograms and Java methods
- **explicit heap-dynamic**: Explicit Heap-Dynamic variables are nameless (abstract) memory cells that are allocated and reallocated by explicit run-time instructions specified by the programmer. The main examples are dynamic objects in C++ (via new and delete) and all objects in Java
- **implicit heap-dynamic**: Implicit Heap-Dynamic variables are bound to heap storage only when they are assigned values. Allocation and release occur when values are reassigned to variables. As a result, Implicit heap-dynamic variables have the highest degree of flexibility. The main examples are some variables in JavaScript, PHP and all variables in APL

#### Why do I need to know this?

- You are studying a type of scripting language, and this is common knowledge that is needed to know.
- When acknowledging yourself to other languages, these basics will give you an advantage.

---

## Variables in Bash

#### Variable scope

In Bash, the scope of user variables is generally global. That means, it does not matter whether a variable is set in the "main program" or in a "function", the variable is defined everywhere.

Compare the following equivalent code snippets
```sh
myvariable=test

echo $myvariable

myfunction() {
  myvariable=test
}
 
myfunction

echo $myvariable
```

##### Local variables

Bash provides ways to make a variable's scope local to a function:

- Using the `local` keyword, or
- Using `declare` (which will detect when it was called from within a function and make the variable(s) local).

```sh 
myfunc() {
local var=VALUE
 
# alternative, only when used INSIDE a function
declare var=VALUE
}
```

The `local` keyword (or declaring a variable using the declare command) tags a variable to be treated completely local and separate inside the function where it was declared: 

```sh 
foo=external
 
print_value() {
local foo=internal
 
echo $foo
}
 
 
# this will print "external"
echo $foo
 
# this will print "internal"
print_value
 
# this will print - again - "external"
echo $foo
```

#### Environment variables

The environment space is not directly related to the topic about scope, but it's worth mentioning.

Every UNIX process has a so-called environment. Other items, in addition to variables, are saved there, the so-called environment variables. When a child process is created (in Bash e.g. by simply executing another program, say ls to list files), the whole environment including the environment variables is copied to the new process. Reading that from the other side means: Only variables that are part of the environment are available in the child process.

A variable can be tagged to be part of the environment using the export command:
```sh
# create a new variable and set it:
# -> This is a normal shell variable, not an environment variable!
myvariable="Hello world."
 
# make the variable visible to all child processes:
# -> Make it an environment variable: "export" it
export myvariable
```


#### Bash Specific benefits with variables

One core functionality of Bash is to manage parameters. A parameter is an entity that stores values and is referenced by a name, a number or a special symbol.

- parameters referenced by a name are called variables (this also applies to arrays)
- parameters referenced by a number are called positional parameters and reflect the arguments given to a shell
- parameters referenced by a special symbol are auto-set parameters that have different special meanings and uses

Parameter expansion is the procedure to get the value from the referenced entity, like expanding a variable to print its value. On expansion time you can do very nasty things with the parameter or its value. These things are described here.

If you saw some parameter expansion syntax somewhere, and need to check what it can be

Arrays can be special cases for parameter expansion, every applicable description mentions arrays later.

Looking for a specific syntax you saw, without knowing the name? 

- Simple usage
    - $PARAMETER
    - ${PARAMETER}
- Indirection
    - ${!PARAMETER}
- Case modification
    - ${PARAMETER^}
    - ${PARAMETER^^}
    - ${PARAMETER,}
    - ${PARAMETER,,}
    - ${PARAMETER~}
    - ${PARAMETER~~}
- Variable name expansion
    - ${!PREFIX*}
    - ${!PREFIX@}
- Substring removal (also for filename manipulation!)
    - ${PARAMETER#PATTERN}
    - ${PARAMETER##PATTERN}
    - ${PARAMETER%PATTERN}
    - ${PARAMETER%%PATTERN}
- Search and replace
    - ${PARAMETER/PATTERN/STRING}
    - ${PARAMETER//PATTERN/STRING}
    - ${PARAMETER/PATTERN}
    - ${PARAMETER//PATTERN}
- String length
    - ${#PARAMETER}
- Substring expansion
    - ${PARAMETER:OFFSET}
    - ${PARAMETER:OFFSET:LENGTH}
- Use a default value
    - ${PARAMETER:-WORD}
    - ${PARAMETER-WORD}
- Assign a default value
    - ${PARAMETER:=WORD}
    - ${PARAMETER=WORD}
- Use an alternate value
    - ${PARAMETER:+WORD}
    - ${PARAMETER+WORD}
- Display error if null or unset
    - ${PARAMETER:?WORD}
    - ${PARAMETER?WORD}

---

### Variable Names good practices 

 According to [Uncle Bob](https://en.wikipedia.org/wiki/Robert_C._Martin) variables and function names means a lot to a developer/sysadmin that is going to inherit your work, thus there are sets of rules that one can implement while writing scripts and software that may guide you through decision making process. 
 Here is a short recap:

 - Choose your names thoughtfully: don't use short names and tool long names
 - Communicate your intent : variables and function should be read as a story
 - Avoid Disinformation: 
 - Pronounceable names : variable named *PC_GWDA* will be hard to explain to others
 - Avoid Encodings: use dosToUnix and UnixToDos or learn to use sed tool for removing encodings
 - Choose parts of speech well: give variable noun names and functions verb names, if boolean, make them predicates.
 - The scope rules:
     - Variable names should be short if they are in tiny scope (vars used only in one place)
     - Variable names should be long  if they are in large scope
     - Environment/Global variables should be capitalized and long names
     - Function should have short name if they have long scope
     - Function should have long and descriptive name if they short scope
Here a link to playlist of his [book](https://www.youtube.com/watch?v=7EmboKQH8lM&list=PLmmYSbUCWJ4x1GO839azG_BBw8rkh-zOj)

Remember [Martin Fowlers](https://en.wikipedia.org/wiki/Martin_Fowler_(software_engineer)) *law: Any fool can write a code that `computer` understands, yet it takes good programmer to write a code that `HUMAN` understands*
I, myself, enjoyed the course very much, hope you will too

<div style="page-break-after: always; visibility: hidden"> </div>

---

# Practice

- Where is the bash program located on your system?
- Use the --version option to find out which version you are running.
- Which shell configuration files are read when you login to your system using the graphical user interface and then opening a terminal window?
- Are the following shells interactive shells? Are they login shells?
    - A shell opened by clicking on the background of your graphical desktop, selecting "Terminal" or such from a menu.
    - A shell that you get after issuing the command ssh localhost.
    - A shell that you get when logging in to the console in text mode.
    - A shell obtained by the command xterm &.
    - A shell opened by the mysystem.sh script.
    - A shell that you get on a remote host, for which you didn't have to give the login and/or password because you use SSH and maybe SSH keys.
- Can you explain why bash does not exit when you type Ctrl+C on the command line?
- Display directory stack content.
- Display hashed commands for your current shell session.
- How many processes are currently running on your system? Use ps and wc, the first line of output of ps is not a process!
- How to display the system hostname? Only the name, nothing more!

- This exercise will help you to create your first script.
    - Write a script using your favorite editor. The script should display the path to your home directory and the terminal type that you are using. Additionally it shows all the services started up in runlevel 3 on your system. (hint: use HOME, TERM and ls /etc/rc3.d/S*)
    - Add comments in your script.
    - Add information for the users of your script.
    - Change permissions on your script so that you can run it.
    - Run the script in normal mode and in debug mode. It should run without errors.
    - Make errors in your script: see what happens if you misspell commands, if you leave out the first line or put something unintelligible there, or if you misspell shell variable names or write them in lower case characters after they have been declared in capitals. Check what the debug comments say about this.


<div style="page-break-after: always; visibility: hidden"> </div>
