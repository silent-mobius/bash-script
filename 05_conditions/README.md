
---

<center style="background-color:lightgreen">
    <h1 style="color:black"> 
     Bash: Scripting Conditions
    </h1>
</center>

---

## Conditions

Condition check in shell script is usually done with `test` command, which was converted to square brackets \[\] in kornshell. `test` command checks the exit status of the last command, provides the shell itself with exit status number.
Last command exit code can be verified with with output `$?` command.

```sh
ls -l /tmp
echo $?
ls /root
echo $?
```

In case you are wondering there 0-255 possible exit codes: 0 being success and 1-255 mean different levels of fails
The more explained list of exit codes can be found [here](https://linux.die.net/abs-guide/exitcodes.html)


---

## The if-clause

The if-clause can control the script's flow (what's executed) by looking at the exit codes of other commands. 
At times you need to specify different courses of action to be taken in a shell script, depending on the success or failure of a command. The if construction allows you to specify such conditions.

The most compact syntax of the if command is:

``` sh
if TEST-COMMANDS; 
then 
    CONSEQUENT-COMMANDS; 
fi
```
The *TEST-COMMAND* list is executed, and **if its return status is zero**, the *CONSEQUENT-COMMANDS* list is executed. The return status is the exit status of the last command executed, or zero if no condition tested true.
[In the tests.sh](./0_tests.sh) you can find examples of tests that you can perform

### The if-else-clause

The `if` <LIST> commands are executed. If the exit code was 0 (TRUE) then the then <LIST> commands are executed, otherwise the `else` <LIST> commands and their then <LIST> statements are executed in turn.

```sh
if TEST-COMMANDS; 
then 
    CONSEQUENT-COMMANDS; 
else  
    OTHER-CONSEQUENT-COMMANDS;
fi
```

### The elif-clause


The `if` <LIST> commands are executed. If the exit code was 0 (TRUE) then the then <LIST> commands are executed, otherwise the `elif` <LIST> commands and their then <LIST> statements are executed in turn, if all down to the last one fails, the else <LIST> commands are executed, if one of the `elif` succeeds, its then thread is executed, and the if-clause finishes.

Basically, the `elif` clauses are just additional conditions to test (like a chain of conditions) if the very first condition failed. If one of the conditions fails, the else commands are executed, otherwise the commands of the condition that succeeded.

```sh
if TEST-COMMANDS; 
then 
    CONSEQUENT-COMMANDS; 
elif  OTHER-TEST-COMMANDS;
then
    OTHER-CONSEQUENT-COMMANDS;
fi
```


In [basic condition script](./1_basic_conditions.sh) you have examples of variables that can be passed to script and made be useful. There is a intentional error: see if you can fix it.

<div style="page-break-after: always; visibility: hidden"> </div>

---

# Practice

- Write a script that receives four parameters, and outputs them in reverse order.
- Write a script that receives two parameters (two filenames) and outputs whether those files exist.
- Write a script that asks for a filename. Verify existence of the file, then verify that you own the file, and whether it is writable. If not, then make it writable.


<div style="page-break-after: always; visibility: hidden"> </div>
