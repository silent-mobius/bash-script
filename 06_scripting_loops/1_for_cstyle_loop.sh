#!/usr/bin/env bash

########################################################################
#Created by: Silent-Mobius
#Purpose: Exercise
#Date: 01.03.2021
#version: 1.0.0
set -o pipefail # should stop script when a | b, either a or b fails
set -o errexit # should stop script on any error
set -o noclobber # should not allow to over-write script
set -o nounset # do not allow un-used variables
########################################################################



frontend_proxy_lan_ips=("10.10.29.72" "10.10.29.71" "10.10.29.70" "10.10.29.69" "10.10.29.68")
tmp_conf="/tmp/lighttpd.backend.conf.$$"
array_size=${#frontend_proxy_lan_ips[*]}        
last_item=""
 
echo '### Log real client ips on all backends ###' >"$tmp_conf"
echo -e 'server.modules += ( "mod_extforward" ) 
extforward.headers = ("X-Forwarded-For") 
extforward.forwarder = ('   >> "$tmp_conf"
 
# For loop 
for (( i=0; i<${array_size}; i++ ));
do
	[ $i -lt $(( $array_size - 1 )) ] && last_item="," || last_item=""      # remove , for last item in an array
        echo "      \"${frontend_proxy_lan_ips[$i]}\" => \"trust\"${last_item} " >>"$tmp_conf"
done
 
echo ')' >>"$tmp_conf"
 
# Copy it
cp -f "$tmp_conf" /etc/lighttpd/
 
# remove temp file
[ -f "$tmp_conf" ] && rm -f "$tmp_conf"