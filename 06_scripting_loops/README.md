
---

<center style="background-color:lightgreen">
    <h1 style="color:black"> 
     Bash: Scripting Loops
    </h1>
</center>


### Loops

Often we want to run the same sequence of commands, over and over again, with slight differences. This is where,Bash loop come in handy.
Bash loops are very useful. In this section of our Bash Scripting Tutorial we'll look at the different loop formats available to us as well as discuss when and why you may want to use each of them.
Loops allow us to take a series of commands and keep re-running them until a particular situation is reached. They are useful for automating repetitive tasks.
There are **4 basic** loop structures in Bash scripting which we'll look at below:

- [for](#for)
- [while](#while)
- [until](#until)
- [select](#select)

There are also a few statements which we can use to control the loops operation.


#### For

The for quite simple and different from other loop types. What it does is say for each of the items in a given list, perform the given set of commands. It has the following syntax.
``` bash
for var in <list>
    do
         <commands>
    done
```
The for loop will take each item in the list (in order, one after the other), assign that item as the value of the variable var, execute the commands between do and done then go back to the top, grab the next item in the list and repeat over.
The list is defined as a series of strings, separated by spaces, as can be seen [here](./0_for_loops.sh)

---

#### For (C-Style)

The C-style for-loop is a compound command derived from the equivalent [ksh88](https://en.wikipedia.org/wiki/KornShell) feature, which is in turn derived from the C `for` keyword. Its purpose is to provide a convenient way to evaluate arithmetic expressions in a loop, plus initialize any required arithmetic variables. It is one of the main "loop with a counter" mechanisms available in the language.

```sh 
for (( <EXPR1> ; <EXPR2> ; <EXPR3> )) do
  <LIST>
done
```

Here is [link to a script](./1_for_cstyle_loop.sh) that provides practical example c-style for loop. please go over it to verify that you understand the syntax and use case.

---

#### While

One of the easiest loops to work with is `while` loops. They say, while an expression is `true` (exit code of existing test is 1), keep executing these lines of code. They have the following format:
```sh
while [[ <test> ]]
    do
        <commands>
    done
```

You'll notice that `while` loop is similar to `if` statements, because of  the `test` statement that is placed between square brackets \[ \]. That is why it is called in some of cases `condition loop`, yet not unlike `if` condition, `while` will repeat itself until the condition in square brackets is met. In [while scripts](./2_while_loops.sh) you have examples of conditioning that can be used.

> Note: The difference is that `if` statement happens only once, but with `while` loop goes on forever.

---

#### Until

Relatively simple, `until` loop, is exactly as it does: it executes the `<test>` and if the exit code of it was not 0 (FALSE) it executes `<commands>`. This happens again and again until `<true>` returns TRUE.

This is exactly the opposite of the while loop. 
```sh
until [[ <test> ]]
    do
        <commands>  
    done
```
As before, until loop is also some what alike `if not` statement.

Here are some simple links to [until loop example](./3_until_loops.sh)


---

#### Select

**This syntax is not well documented and depending on version of shell that you use, will behave differently. It is suggested NOT to use.**

This compound command provides a kind of menu. The user is prompted with a numbered list of the given words, and is asked to input the index number of the word. If a word was selected, the variable <NAME> is set to this word, and the list <LIST> is executed.

If no in <WORDS> is given, then the positional parameters are taken as words (as if in "$@" was written).

Regardless of the functionality, the number the user entered is saved in the variable REPLY
Bash knows an alternative syntax for the select command, enclosing the loop body in `{...}` instead of `do ... done`: 

```sh
select x in 1 2 3
{
  echo $x
}
```
> Note: practical examples of [select loop can be found here](./4_select_loops.sh)

---

### Controlling Loops: `break` and `continue`

Usually when even you implement loops of any kind, you'd prefer to run in sequential manner.However there will be cases where we'll need to intervene and alter their running slightly. There are two statements we may issue to do this: 
- [break](#break)
- [continue](#continue)


#### Break

The break statement tells Bash to leave the loop straight away. It may be that there is a normal situation that should cause the loop to end but there are also exceptional situations in which it should end as well. For instance, maybe we are copying files but if the free disk space gets below a certain level we should stop copying. Check [this example](./5_break.sh) to see it in action.

#### Continue

The continue statement tells Bash to stop running through this iteration of the loop and begin the next iteration. Sometimes there are circumstances that stop us from going any further. For instance, maybe we are using the loop to process a series of files but if we happen upon a file which we don't have the read permission for we should not try to process it.
Check [Continue script example](./6_continue.sh) to see it in action.

---

### Nested Loops

Nested loop are a very simple concept: loop inside of a loop. e.g.

```sh
for (( i = 1; i <= 5; i++ ))      ### Outer for loop ###
do

    for (( j = 1 ; j <= 5; j++ )) ### Inner for loop ###
    do
          echo -n "$i "
    done

  echo "" #### print the new line ###
done

```

For each value of i the inner loop is cycled through 5 times, with the variable j taking values from 1 to 5. The inner for loop terminates when the value of j exceeds 5, and the outer loop terminates when the value of i exceeds 5. 

<div style="page-break-after: always; visibility: hidden"> </div>

---

### Practice

1. create a script that will:

- a.check that your user is in sudo/wheel group of users.
- b.check that your home directory is at /home.
- c.that your nick name is set.

2. create a script that will:

- a. creates a hidden file named user_info.sh
- b. puts your details in it: name, last-name, ID, DOB, POB.
- c. make the the file print every time you login.(check about .bashrc file in your home folder.)

3. create script that will:

- a. use "for" loop to see all the "services" in /etc/systemd/system.
- b. if there are other types of files (target/wants....etc) put them in file under /tmp folder, in file named "systemd.misc"

4. create a script that will print triangle using "*" sign.
5. create a script that will print reverse triangle using "*" sign with "for" loop.
6. create a script that will print diamond (triangle + reverse triangle) using "*" with "for" loop.
7. create a script that will use "df" command and "for" loop will print only disks with low space.
8. create a script that will print chess board (black/white cubes).


<div style="page-break-after: always; visibility: hidden"> </div>
