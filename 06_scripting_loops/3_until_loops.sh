#!/usr/bin/env bash

########################################################################
#Created by: Silent-Mobius
#Purpose: Exercise
#Date: 01.03.2021
#version: 1.0.0
set -o pipefail # should stop script when a | b, either a or b fails
set -o errexit # should stop script on any error
set -o noclobber # should not allow to over-write script
set -o nounset # do not allow un-used variables
########################################################################


count=10
i=20
 
# until loop with single condition
until [ $i -lt $count ]; do
   echo "$i"
   let i--
done

sleep 2 

count=10
a=20
b=16
 
# until loop for multiple conditions in expression
until [[ $a -lt $count || $b -lt count ]]; do
   echo "a : $a, b : $b"
   let a--
   let b--
done