#!/usr/bin/env bash

########################################################################
#Created by: Silent-Mobius
#Purpose: Exercise
#Date: 01.03.2021
#version: 1.0.0
########################################################################



for value in $1/*
    do
            if [[ ! -r $value ]];then
                   echo "$value not readable" 1>&2
                continue
            fi
        cp $value $1/backup/
    done