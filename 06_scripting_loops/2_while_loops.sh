#!/usr/bin/env bash

########################################################################
#Created by: Silent-Mobius
#Purpose: Exercise
#Date: 01.01.2020
#version: 1.0.0
set -o pipefail # should stop script when a | b, either a or b fails
set -o errexit # should stop script on any error
set -o noclobber # should not allow to over-write script
set -o nounset # do not allow un-used variables
########################################################################


i=0
while [ $i -le 10 ];
do
	echo Counting down, from 0 to 10, now at $i;
	(( i++ ));sleep 1
done

sleep 2

i=10

until [ $i = 0 ];
do
    echo running until loop till i get to 0 , now at $i;
    let i--;sleep 1
done

sleep 2

#IFS stands for Internal field Seperator

file=/etc/resolv.conf
# set field separator to a single white space 
while IFS=' ' read -r f1 f2
do
	echo "field # 1 : $f1 ==> field #2 : $f2"
done < "$file"

