#!/usr/bin/env bash

########################################################################
#Created by: Silent-Mobius
#Purpose: Exercise
#Date: 01.01.2020
#version: 1.0.0
set -o pipefail # should stop script when a | b, either a or b fails
set -o errexit # should stop script on any error
set -o noclobber # should not allow to over-write script
set -o nounset # do not allow un-used variables
########################################################################


for  i in 1 2 3 4 5 6 7
    do
        echo  $i
    done

sleep 2

echo "using  \{\} for expention of range"
 for counter in {1..20}
    do
	echo  counting  from 1 to 20 , now at  $counter
	    sleep 1
    done 

sleep 2

echo "using  \$\(\) for sub-shell expantion"
for counter in  $(seq 1 20)
    do  
        echo  counting  from 1 to 20 , now at  $counter
            sleep 1
    done  

sleep 2

echo "nested loops"

for counter in {1..20}  
    do 
        for inner_counter in {1..10}
            do
                echo "$counter & $inner_counter"
            done
        echo exiting inner loop 
    done